# See more info at: https://hub.docker.com/_/python
FROM python:latest

# Update apps on the base image
RUN apt-get -y update && apt-get install -y

# Install dev niceties (ZSH)
RUN apt-get -y install zsh
# Change default shell to ZSH
RUN chsh -s $(which zsh)
# Install Oh My Zsh
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
# ZSH config
RUN rm ~/.zshrc
COPY dotfiles /root/

# Python lib install
RUN pip install coinapi.rest.v1	python-dotenv

WORKDIR /app

CMD ["/usr/bin/zsh"]