#!/bin/sh

APP_NAME="dev:trading-boy"
PATH_IN_LOCAL_ENV=/home/ahaas/workspace/trading-boy

docker build -t $APP_NAME . --file $PATH_IN_LOCAL_ENV/Dockerfile
docker run -it -P -v $PATH_IN_LOCAL_ENV:/app $APP_NAME