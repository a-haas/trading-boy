from coinapi_rest_v1.restapi import CoinAPIv1
import datetime

import env as ENV

test_key = ENV.API_KEY

api = CoinAPIv1(test_key)

start = (datetime.datetime.now() 
    + datetime.timedelta(days=-8)
).strftime('%Y-%m-%d')

orderbooks_historical_data_btc_usd = api.orderbooks_historical_data(
    'BITSTAMP_SPOT_BTC_USD', 
    {'time_start': start}
)

for data in orderbooks_historical_data_btc_usd:
    print('Symbol ID: %s' % data['symbol_id'])
    print('Time Exchange: %s' % data['time_exchange'])
    print('Time CoinAPI: %s' % data['time_coinapi'])
    print('Asks:')
    for ask in data['asks']:
        print('- Price: %s' % ask['price'])
        print('- Size: %s' % ask['size'])
    print('Bids:')
    for bid in data['bids']:
        print('- Price: %s' % bid['price'])
        print('- Size: %s' % bid['size'])